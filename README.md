## Ticket sales APP

### Install APP

### You need installed docker-compose and nodejs with npm

**1 - Clone app from repository and enter to the project folder - default `ticket-sales`**
```
git clone https://bitbucket.org/alex_zaytseff/ticket-sales.git
cd ./ticket-sales
```
**2 - Build fronten from UI folder**
```
cd ./ui
npm install
npm run build
```
**3 - Return to project root and copy compilled bundle into server public folder**
```
cd ../
cp -r ./ui/build/* ./api/public
```

**4 - Start app with docker-compose**

with server output use:
```
docker-compose up
```
or as daemon use:
```
docker-compose up -d
```

**5 - Open browser and use**
```
http://localhost:3001
```

**6 - Enjoy!**