'use strict';

const Path = require('path');
const Hapi = require('hapi');
const Inert = require('inert');

const routes = require('./src/routes.js');

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: '0.0.0.0',
        routes: {
            cors: true,
            files: {
                relativeTo: Path.join(__dirname, 'public')
            }
        },
    });

    // Add static files suport
    await server.register(Inert);

    // Add routes from routes list module
    server.route(routes.list);
    
    await server.start();
    
    console.log('Server running on %ss', server.info.uri);
}

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();