const api_version = require('./version');
const data = require('../fakedata.json');
const route_list = [
  // Static files support for frontend bundle
  {
    method: 'GET',
    path: '/{param*}',
    handler: {
        directory: {
            path: './'
        }
    }
  },
  // Api methods
  {
      method: 'GET',
      path:'/api',
      handler: (request, h) => {
          return {'version' : api_version};
      }
  },
  // Events
  {
      method: 'GET',
      path:'/api/v1/events',
      handler: (request, h) => {
        return data.events;
      }
  },
  {
      method: 'GET',
      path:'/api/v1/events/{id}',
      handler: (request, h) => {
        var event = null;
        data.events.forEach(function(item) {
            if(item.id.toString() === request.params.id) {
                event = item;
            }
        });
        
        return (event) ? event : h.response({'error' : 'Event not found'}).code(404);
      }
  },
  // Users
  {
      method: 'GET',
      path:'/api/v1/users',
      handler: (request, h) => {
        return data.users;
      }
  },
  {
      method: 'GET',
      path:'/api/v1/users/{id}',
      handler: (request, h) => {
        var user = null;
        data.users.forEach(function(item) {
            if(item.id.toString() === request.params.id) {
                user = item;
            }
        });
        
        return (user) ? user : h.response({'error' : 'User not found'}).code(404);
      }
  },
  // 404 err handler
  {
      method: '*',
      path: '/{any*}',
      handler: function (request, h) {

          return h.response({'error' : 'Bad request'}).code(400);
      }
  }
];

exports.list = route_list;