import axios from 'axios'
import {
  GET_EVENT_DETAILS_REQUEST,
  GET_EVENT_DETAILS_SUCCESS,
  GET_EVENT_DETAILS_FAILURE,
} from '../../constants/Events'

// Get event details

export function getEventDetails(id) {
  //console.log('id', id)
  return async (dispatch) => {
    function onFetch(id) {
      dispatch({
        type: GET_EVENT_DETAILS_REQUEST,
        payload: null,
      })
    }
    onFetch(id)

    function onFailure (error) {
      dispatch({
        type: GET_EVENT_DETAILS_FAILURE,
        error: error
      })
    }

    function onSuccess(event){
      dispatch({
        type: GET_EVENT_DETAILS_SUCCESS,
        payload: event.data,
      })
      return event
    }
    try {
      const event = await axios.get('http://localhost:3001/api/v1/events/' + id)
      return onSuccess(event)
    } 
    catch (error) {
      return onFailure(error)
    }
  }
}