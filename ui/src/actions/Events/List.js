import axios from 'axios'
import {
  GET_EVENTS_LIST_REQUEST,
  GET_EVENTS_LIST_SUCCESS,
  GET_EVENTS_LIST_FAILURE,
} from '../../constants/Events'

// Get Events List
export function getEventsList() {
  return async dispatch => {
    function onFetch() {
      dispatch({
        type: GET_EVENTS_LIST_REQUEST,
        payload: [],
      })
    }
    onFetch()

    function onFailure (error) {
      dispatch({
        type: GET_EVENTS_LIST_FAILURE,
        error: error
      })
    }

    function onSuccess(eventsList){
      // console.log(eventsList.data)
      dispatch({
        type: GET_EVENTS_LIST_SUCCESS,
        payload: eventsList.data,
      })
      return eventsList
    }
    try {
      const eventsList = await axios.get('http://localhost:3001/api/v1/events')
      return onSuccess(eventsList)
    } 
    catch (error) {
      return onFailure(error)
    }
  }
}
