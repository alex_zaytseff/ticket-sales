import React from 'react';
import { Navbar, Nav } from "react-bootstrap";

const Menu = () => {
  return (
    <Navbar bg="light" expand="lg" className="mb-5">
      <Navbar.Brand>
        Simple Tiket Sales App
      </Navbar.Brand>
      <Nav.Link href="/events">
        Events List
      </Nav.Link>
    </Navbar>
  )

}

export default Menu