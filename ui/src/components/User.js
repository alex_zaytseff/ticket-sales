import React from 'react'
import PropTypes from 'prop-types'

const User = ({name}) => {
  return (
    <div>
      <p>Hello, {name}!</p>
    </div>
  )
}

User.propTypes = {
  name: PropTypes.string.isRequired,
}

export default User