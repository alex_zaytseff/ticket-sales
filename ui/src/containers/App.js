import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Redirect, Switch} from 'react-router-dom'
import { connect } from 'react-redux'

import Header from '../components/Header'
import EventsList from '../containers/EventsList'
import EventDetails from '../containers/EventDetails'
import User from '../components/User'

import { Container, Row, Col } from "react-bootstrap"
import "bootswatch/dist/journal/bootstrap.min.css"

// Show APP with events list
class App extends Component {

  render() {
    const { user } = this.props
    return (
      <div>
        <Router>
          {/* {console.log('from router', window.location)} */}
          <Header/>
          <User name={user.name}/>
          <Container>
            <Row>
              <Col md={12} sm={12}>
                <Switch>
                  <Route exact path={'/events'} component={EventsList}/>
                  <Route path="/events/:id" component={EventDetails}/>
                  <Redirect from="/" to="/events" />
                </Switch>
              </Col>
            </Row>
          </Container>
        </Router>
      </div>
    );
  }
}

// Convert store data to componen props
const mapStateToProps = ({ user }) => {
  return {
    user: user,
  }
}

export default connect(
  mapStateToProps,
)(App);
