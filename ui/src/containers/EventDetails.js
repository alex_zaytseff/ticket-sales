import React, { Component } from 'react';
import { connect } from 'react-redux'
import { getEventDetails } from '../actions/Events/Details'
import "bootswatch/dist/journal/bootstrap.min.css";


// Module show event details
//const EventDetails = (event) => {
class EventDetails extends Component {
  componentDidMount () {
    this.props.getEventDetailsAction()
  }

  render () {
    console.log('details', this.props)
    const event = this.props.event
    let data;
    if (!event) {
      data = <div>Loading</div>
    }
    else {
      let eventPicture = "http://localhost:3001/img/event" + event.id + ".png";
      data =    <div>
        <h4>{event.title ? event.title : 'No title'}</h4>
        <p>Description: {event.description ? event.description : 'No description'}</p>
        <p>Date: {event.date}</p>
        <p>All tickets {event.tickets_total}</p>
        <p>Tickets left {event.tickets_left}</p>
        <p>COST per ticket {event.tickets_cost}</p>
        <img src={eventPicture} alt={event.title} />
      </div>
    }

    
    return (
      <div>
        <h3>
          Event details
        </h3>
        {data}
      </div>
    );
  }
}
// Convert store data to componen props
const mapStateToProps = ({events: {current}}) => {
  console.log('event', current)
  return {
    event: current,
  }
}

const mapDispatchToProps = (dispatch, id) => ({
  getEventDetailsAction: event => dispatch(getEventDetails(id.match.params.id)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EventDetails)
