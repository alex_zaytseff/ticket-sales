import React, { Component } from 'react'
import { getEventsList } from '../actions/Events/List'
import { connect } from 'react-redux'
import { Nav } from "react-bootstrap"


class EventsList extends Component {
  componentDidMount () {
    this.props.getEventsListAction()
  }
  
  render() {
    
    const events = this.props.events;
    let data
    if (!events) {
      data = <div>Loading</div>
    }
    else {
      data = <Nav  
      variant="pills"
      className="flex-column" 
    >
      {events.map((event, index) => (
        <Nav.Link key={index} eventKey={index} href={"/events/" + event.id}>{event.title}</Nav.Link>
      ))}
    </Nav>
    }


    return (

      <div>
        <h3>Events list</h3>
        {data}
      </div>
    )
  }
}

// Convert store data to componen props
const mapStateToProps = ({events: {list}}) => {
  return {
    events: list,
  }
}

const mapDispatchToProps = dispatch => ({
  getEventsListAction: eventsList => dispatch(getEventsList()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EventsList)
