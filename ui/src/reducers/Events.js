import {
  GET_EVENTS_LIST_REQUEST,
  GET_EVENTS_LIST_SUCCESS,
  GET_EVENTS_LIST_FAILURE,
  GET_EVENT_DETAILS_REQUEST,
  GET_EVENT_DETAILS_SUCCESS,
  GET_EVENT_DETAILS_FAILURE,
} from '../constants/Events'

// initial state for events
export const initialState = {
  list: null,
  current: null,
}

// export event reducer
export function eventReducer(state = initialState, action) {
  switch (action.type) {
    // LIST
    case GET_EVENTS_LIST_REQUEST:
      return { ...state, list: null, isFetching: true }
    case GET_EVENTS_LIST_SUCCESS:
      return { ...state, list: action.payload, isFetching: false }
    case GET_EVENTS_LIST_FAILURE:
      return { ...state, list: action.payload, isFetching: false, error: action.error }

    // Details
    case GET_EVENT_DETAILS_REQUEST:
      return { ...state, current: null, isFetching: true }
    case GET_EVENT_DETAILS_SUCCESS:
      return { ...state, current: action.payload, isFetching: false }
    case GET_EVENT_DETAILS_FAILURE:
      return { ...state, current: null, isFetching: false, error: action.error }
    default:
      return state
  }
}