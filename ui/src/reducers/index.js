import { combineReducers } from 'redux'
import { eventReducer } from './Events'
import { userReducer } from './User'

export default combineReducers({
  events: eventReducer,
  user: userReducer,
})
